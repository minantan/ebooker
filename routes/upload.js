var async = require('async')
  , path = require('path')
  , fs = require('fs')
  , pdftotext = require('../pdftotext')
  , amazon = require('../amazon')
  , utils = require('../utils')
  , config = require('../config')
  , norchindexer = require('../indexer/norchindexer')
  , pdfMetadata = require('../metadata_extractors/pdfMetadata')
  , filename = require('../metadata_extractors/filename')
  , Book = require('../models/book');


var metadataExtractors = function(){
  return [    
    pdfMetadata.extractMetadata, 
    filename.extractMetadata
  ];
}

var extractPDFMetadata = function(file, callback) {
  utils.generateHash(file.path, 'md5', function(err, md5) {
    var book = Book.create()
      , pdf = {
          'file': file,
          'book': book
        }
    pdf.book.md5 = md5;

    var extractors = [ utils.identityCallback(pdf) ].concat(metadataExtractors());
    // each function expects: { file: req.files file, book: currentPDFInfo }
    async.waterfall(extractors, callback);  
  });
}

var retrieveInfo = function(file, callback) {
  extractPDFMetadata(file, function(err, infoResult) {

    // no useful info to save
    if (err) throw err;

    var book = infoResult.book;

    amazon.retrieve(book.author, book.title, function(err, result) {
      
      if (!err && result !== null) {
        book.title = result.title;
        book.author = result.author;
        book.rating = result.rating;
        book.cover = result.cover;
        book.description = result.description;
      }

      return callback(null, {
        'file': infoResult.file,
        'book': book
      });
    });
  });
}

var validate = function(file, callback) {
  if (file === null || file.type !== 'application/pdf') {
    return callback(new Error('Only pdfs are allowed'), null);
  }
  utils.generateHash(file.path, 'md5', function(err, result){
    Book.findOne({ 'md5': result }, { 'title': 1, 'author': 1 }, { lean: true }, function(err, book) {
      if (book) {
        return callback(new Error('File already exists in the system as ' + book.title), null);
      } else {
        return callback(err, file);
      }
    });
  });
}

var copyFileToStore = function(file, id, callback) {
  var targetDir = path.join(config.storage, id);
  utils.createIfNotExists(targetDir, function(err) {
    
    if (err) {
      return callback(err, null);
    } else {
      var fileLocation = path.join(targetDir, id + '.pdf')
        , outStream = fs.createWriteStream(fileLocation)
                        .on("finish", function(ex) {
                          return callback(null, fileLocation);
                        })
                        .on("error", function(ex) {
                          return callback(ex, null);
                        });

      fs.createReadStream(file.path).pipe(outStream);
    }
  });
}

var saveToDB = function(record, callback) {
  console.log("Saving " + record.file.name + " to db");
  return Book.save(record.book, function(err, savedBook) {

    var idString = savedBook._id.toString();

    return copyFileToStore(record.file, idString, function(err, result) {
      if (err) {
        console.error("error occurred, removing " + idString + " from db. Error was " + err);      
        Book.remove(savedBook);
        return callback(err, null);
      } else {
        savedBook.filename = record.file.name;
        return Book.save(savedBook, function(err, res) {
          record.book = res;
          return callback(null, record);
        });
      }
    })  
  })
}

var createDocumentForIndexing = function(record, callback) {
  // Has the fortunate side effect of generating a txt
  pdftotext.convert(record.file.path, function(err, result){

    if (err) {
      return callback(err, null);
    }

    var book = record.book
      , idString = book._id.toString();

    fs.writeFile(utils.getFilePathFromId(idString, 'txt'), result, function(err) {
      if (err) {
        console.log('Unable to write ' + record.file.name + ' to txt file - ' + err);
      }
    }); 

    var indexRecord = Book.createForIndexing(book, result);

    return callback(null, { 'id': idString, 'indexRecord': indexRecord });

  });
}

var renderResults = function(response) {
  return function(err, result) {
    console.log("results " + result);
    var books = []
      , errors = [];

    if (Array.isArray(result)) {
      result.forEach(function(elt, index, arr){
        if (utils.isError(elt)) {
          errors.push(elt);
        } else if (elt !== null && elt.book !== null) {
          books.push(elt.book);
        }      
      })
    } 

    return response.render('bookdata', {
      'books': books,
      'errors': errors
    });
  }
}

var process = function(file, callback) {
  var pipeline = [ 
    async.apply(validate, file), 
    retrieveInfo, 
    saveToDB
  ];
  async.waterfall(pipeline, utils.convertErrToResultErrorHandler(callback));
}

var create = function(req, response){

  var pdfFile = req.files['fileToUpload'];

  if (!pdfFile) {
    return renderResults(response)(null, null);
  } 

  if (!Array.isArray(pdfFile)) {
    pdfFile = [ pdfFile ];    
  } 
  
  var files = pdfFile.map(function(file) {
    return {
      'name': file.name,
      'size': file.size,
      'type': file.type
    }
  });
  async.map(pdfFile, process, function(err, result) {
      // render and index simultaneously
      for (var i = 0; i < result.length; ++i) {
        var elt = result[i];
        if (utils.isError(elt)) {

          files[i].error = elt.message;
        } else {
          files[i].url = '#/books?id=' + result[i].book._id;
        }
      }
      response.send({ 'files': files });

      var validResults = result.filter(function(elt) { 
        return (elt != null && !utils.isError(elt)); 
      });
      // index only valid results
      async.map(validResults, createDocumentForIndexing, function(err, result) {        

        if (err) {
          console.error('during post processing of upload, encountered ' + err);
        } else {
          var ids = []
            , docToIndex = {};

          result.forEach(function(elt, idx, array) {
            ids.push(elt.id);
            docToIndex[elt.id] = elt.indexRecord[elt.id];
          });

          var idBatch = ids.join(', ');
          norchindexer.index(docToIndex, idBatch, { }, function(err, result){
            console.log('Indexing finished for ' + idBatch + ' - ' + result);
          });
        }
      });
    }
  );  
};

var index = function(req, res) {
  res.render('upload', { title: 'Upload PDF' });
};

utils.createIfNotExists(config.storage, function(err) {
  if (err) throw err;
});

exports.create = create;
exports.index = index;