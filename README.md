# EBooker

Ebooker is a simple webapp to maintain a collection of ebooks

To get started:

* Download and install nodejs - http://nodejs.org/
* Download and install MongoDB - http://www.mongodb.org/
* Download and install poppler utils / xpdf
    * Ubuntu/Debian `sudo apt-get install poppler-utils`.
    * MacOSX `sudo port install poppler` or `brew install xpdf`.
    * Windows download and install `Xpdf`. 
* Clone this repo
* Do `npm install` in the repo to get the necessary dependencies
* Ensure that the url mongo is running on is correct in `config.js`
* Do `node app` to bring the app up!

## To use:

* http://localhost:3000/ - browse index

## To run:

* `node app` bring it up
* `make clean` will clean indexes, the store where pdfs and txts are held and drop the DB

## Uses

* MongoDB http://http://www.mongodb.org/
* ExpressJS http://expressjs.com/ with Connect http://www.senchalabs.org/connect/
* AngularJS http://angularjs.org/
* Bootstrap http://getbootstrap.com/
* Search-Index http://github.com/fergiemcdowall/search-index
* Mongoosejs http://mongoosejs.com/
* and various npm modules including pdfinfo, pdftotextjs, pdf, cheerio, async, shelljs, jscoverage, mocha, chai
* jQuery FileUpload https://github.com/blueimp/jQuery-File-Upload
* jQuery http://jquery.com/
* pdf.js http://mozilla.github.io/pdf.js/

## License

(The MIT License)

Copyright (c) 2013 Min'an Tan <tan.minan at gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of a copyright holder shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization of the copyright holder.