// Force test environment
process.env.NODE_ENV = 'test';

var chai = require('chai')
  , jsc = require('jscoverage')
  , path = require('path')
  , pdfToText = jsc.require(module, '../pdftotext')
  , testConfig = require('./testConfig')
  , assert = chai.assert;

chai.Assertion.includeStack = true; 

describe('Parsing PDF text', function() {

  this.timeout(10000);

  it ('should return a string with the pdf text', function(done){
    pdfToText.convert(path.join(testConfig.fixtures, 'nodata.pdf'), function(err, result) {
      if (err) throw err;

      assert.isNotNull(result);
      assert.match(result, /This file should have no author and title specified in its metadata\./);

      done();
    })
  })

});