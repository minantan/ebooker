var shell = require('shelljs');

// we can only do like this because of a bug in shelljs
// global silent
shell.config.silent = true;

function pdftocairo(filename, options) {
  this.options = options || {};
  //quote filename
  this.options.additional = ['"' + filename + '.pdf"'];
  this.filename = filename;

  pdftocairo.prototype.add_options = function(optionArray) {
    if (typeof optionArray.length !== undefined) {
        var self = this;
        optionArray.forEach(function(el) {
          if (el.indexOf(' ') > 0) {
            var values = el.split(' ');
            self.options.additional.push(values[0], values[1]);
          } else {
            self.options.additional.push(el);
          }
        });
    }
    return this;
  };

  pdftocairo.prototype.convert = function(cb) {
    var self = this;
	self.add_options(['"' + self.filename + '"']);
    var child = shell.exec('pdftocairo ' + self.options.additional.join(' '), function(code, data) {
      if (code === 0) {
        if (cb && typeof cb === "function") {
          cb(null, data, self.options.additional);
        }
      }
      else {
        var err;
        if (!shell.which('pdftocairo')) {
          err = new Error('pdftocairo (poppler-utils) is missing. Hint: sudo apt-get install poppler-utils / brew install poppler / port install poppler');
        }
        else {
          err = new Error(data);
        }
        if (cb && typeof cb === "function") {
          cb(err, data, self.options.addtional);
        }
      }
    });
  }

  pdftocairo.prototype.error = function(callback) {
    this.options.error = callback;
    return this;
  };

  pdftocairo.prototype.success = function(callback) {
    this.options.success = callback;
    return this;
  };
}

// module exports
exports = module.exports = function(filename, args) {
  return new pdftocairo(filename, args);
};
