
/**
 * Module dependencies.
 */
var express = require('express')
  , connect = require('connect')
  , routes = require('./routes')
  , user = require('./routes/user')
  , upload = require('./routes/upload')
  , download = require('./routes/download')
  , books = require('./routes/books')
  , bookImages = require('./routes/bookImages')
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose')
  , norch = require('search-index')
  , config = require('./config');

//require('ofe').call();

var app = express();


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(connect.compress());
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.cookieParser('bleah bleh'));
app.use(express.session());


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.configure('test', function() {
  app.use(express.errorHandler({ dumpExceptions: true, showStack:true }));
});

// app.get('/', routes.index);

app.post('/upload', upload.create);

app.get('/download/:id/:type', download.show);
app.get('/download/:id', download.show);

app.get('/books', books.index);
app.put('/books/:id/refresh', books.refresh);
app.get('/books/:id', books.show);
app.put('/books/:id', books.update);
app.delete('/books/:id', books.remove);

app.get('/bookImages/:id', bookImages.show);

mongoose.connect(config.dbUrl);
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

app.get('/', function(req, res) {
  res.sendfile('public/index.html');
});

var server = http.createServer(app).listen(app.get('port'));

exports.server = server;
